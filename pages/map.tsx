import React, { ReactElement, useEffect, useRef, useState } from "react";
import Image from 'next/image';
import Layout from "../components/layout";
import { Popover } from "@mui/material";
import { GetQueries } from 'jotai-query-toolkit/nextjs';
import { appProviderAtomBuilder } from 'micro-stacks/react';
import { StacksMainnet, StacksMocknet, StacksRegtest } from 'micro-stacks/network';
import SvgMap from "../components/map/svgMap";
import ScrollDrag from "../components/map/scrollDrag";
import ReactDOM from "react-dom";
import ScrollContainer from 'react-indiana-drag-scroll'
import AreaPopOver from "../components/popOver/areaPopOver";

type Props = {
    isMobile: boolean;
};


export default function MapPage() {
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
    const open = Boolean(anchorEl);
    const [pos, setPos] = useState({left: 0, top:0});
    const [selectedTile, setSelectedTile] = useState(0);
    const container = useRef(null);
    const [scale, setScale] = useState(1);
    const [scrollSettings, setScrollSettings] = useState({ left: 0, top: 0, transform: 1 });

    useEffect(() => {
        container.current.getElement().scrollTo(3050, 3300);
        container.current.scrollLeft = 3050;
        container.current.scrollTop = 3300;
    }, []);

    const handleClick = (event: any) => {
        console.log(container.current.scrollLeft)
        console.log(container.current.scrollTop)
        if(event.target.attributes.id) {
            setPos({left: event.clientX, top: event.clientY})
            setAnchorEl(event.target);
            setSelectedTile(parseInt(event.target.attributes.id.value))
        }
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    
    const onWheelCapture = (e) => {
        const delta = e.deltaY * -0.0025;
        const newScale = scrollSettings.transform + delta;
  
        const ratio = 1 - newScale / scrollSettings.transform;
        
        if((newScale < 0.625) || (scrollSettings.transform >= 2 && ratio < 0)) {
          // do nothing
        } else {
            setScrollSettings({
                left: scrollSettings.left + (e.clientX - scrollSettings.left) * ratio,
                top: scrollSettings.top + (e.clientY - scrollSettings.top) * ratio,
                transform: newScale,
            });

            // console.log(e.clientX)
            console.log(container.current.scrollLeft + ((950) * ratio))
            console.log(container.current.scrollTop + ((1441) * ratio))
            // console.log((3050) * delta)
            // console.log((3300) * delta)
            console.log(delta)

            container.current.getElement().scrollTo(
                container.current.scrollLeft + e.clientX + ((950) * delta),
                container.current.scrollTop + e.clientY + ((1417) * delta)
            );
        }
    };

    const onScroll = (e) => {
        // console.log(container.current)
        console.log(container.current.scrollLeft);
        console.log(container.current.scrollTop);
    }
      
    //   componentDidMount() {
    //     const node = ReactDOM.findDOMNode(this.container.current)
    
    //     node.scrollTo(scrollMemoryStore.scrollLeft, scrollMemoryStore.scrollTop)
    //   }

    return (
        <ScrollContainer
            ref={container}
            className="scroll-container"
            onScroll={onScroll}
        >
            <ScrollDrag 
                rootClass={'drag-container'}
                onWheelCapture={onWheelCapture}
                scrollSettings={scrollSettings}
            >
                <SvgMap onClick={e => handleClick(e)} />
                <Popover
                    open={open}
                    onClose={handleClose}
                    anchorPosition={pos}
                    anchorEl={anchorEl}
                >
                    <AreaPopOver areaNumber={selectedTile} />
                </Popover>
            </ScrollDrag>
        </ScrollContainer>
        // <>
        //     <ScrollDrag  rootClass={'drag-container'}>
        //         <SvgMap />
        //     </ScrollDrag>
        // </>
    )
}
