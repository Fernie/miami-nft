import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/layout'
import { MicroStacksProvider } from 'micro-stacks/react';

const authOptions = {
  appDetails: {
    name: 'MIA Beachfronts',
    icon: '/some-logo.png'
  }
}

const network = 'mainnet';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MicroStacksProvider  authOptions={authOptions} network={network}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </MicroStacksProvider>
  )
}

export default MyApp
