import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import React from "react"

interface AreaPopOver {
    areaNumber: number;
}

const AreaPopOver: React.FC<AreaPopOver> = ({areaNumber}) => {
    return (
        <Card
            sx={{
                maxWidth: 345,
                minWidth: 250
            }}
        >
            <CardMedia
                component="img"
                height="140"
                image="/static/images/cards/contemplative-reptile.jpg"
                alt="Hex Display"
            />
            <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {`Hex #${areaNumber}`}
                </Typography>
                <Typography variant="h5" component="div">
                    {"Land Title"}
                </Typography>
                <Typography sx={{ fontSize: 20 }} component="div">
                    {`Terrain ${"a"}`}
                </Typography>
            </ CardContent>
        </Card>
    )
}

export default AreaPopOver;