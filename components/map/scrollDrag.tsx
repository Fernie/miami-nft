import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import PropTypes from 'prop-types';


type ScrollDragDefaultProps = {
    rootClass: string,
    onWheelCapture: any,
    scrollSettings: any,
    children: React.ReactNode,
};

export const ScrollDrag: FunctionComponent<ScrollDragDefaultProps> = (props) => {
    const ref = useRef(null);
    const [isScrolling, setIsScrolling] = useState(false);
    const [diffPos, setDiffPos] = useState({ diffX: 0, diffY: 0 });
    const [styles, setStyles] = useState({ left: 0, top: 0 });
    const [clientX, setclientX] = useState(0);
    const [scrollX, setscrollX] = useState(0);
    const [scale, setScale] = useState(1);

    const onDragStart = (e) => {
        const boundingRect = e.currentTarget.getBoundingClientRect();

        setIsScrolling(true);
        setDiffPos({
            diffX: e.clientX - boundingRect.left,
            diffY: e.clientY - boundingRect.top
        });
    };

  const onDragEnd = (e) => {
    const left = e.clientX - diffPos.diffX;
    const top = e.clientY - diffPos.diffY;

    setStyles({ left: left, top: top });
    setIsScrolling(false);
  };

  const onDrag = (e) => {
    if (isScrolling) {
        // e.stopPropagation();
        //   (ref.current as any).scrollLeft = scrollX + e.clientX - clientX;
        //   setscrollX(scrollX + e.clientX - clientX);
        //   setclientX(e.clientX);
    }
  };
    const onScroll = (e) => {
      // console.log(e)
      // console.log(e.clientX)
      // console.log(e.clientY)

      const delta = e.deltaY * -0.0025;
      const newScale = scale + delta;

      const ratio = 1 - newScale / scale;
      
      if((scale < 1 && ratio > 0) || (scale >= 2 && ratio < 0)) {
        // do nothing
      } else {
        setScale(newScale);
        setStyles({
            left: styles.left + (e.clientX - styles.left) * ratio,
            top: styles.top + (e.clientY - styles.top) * ratio
        })
      }

    };

    return (
      <div
        ref={ref}
        onWheelCapture={props.onWheelCapture}
        // onDrag={onDrag}
        // onDragEnd={onDragEnd}
        // onDragStart={onDragStart}
        className={props.rootClass}
        style={{
          // left: props.scrollSettings.left,
          // top: props.scrollSettings.top,
          position: "relative",
          transformOrigin: "0 0",
          transform: `scale(${props.scrollSettings.transform})`
        }}
      >
        {React.Children.map(props.children, child => React.Children.only(child))}
      </div>
    );
}


export default ScrollDrag;